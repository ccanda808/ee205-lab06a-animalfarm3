///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   20_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

      Nene::Nene( string tagID, enum Color newColor, enum Gender newGender ) {
         gender = newGender;                 /// Get from the constructor... not all nene are the same gender (this is a has-a relationship)
         species = "Branta sandvicensis";    /// Hardcode this... all nene are the same species (this is a is-a relationship)
         featherColor = newColor;            /// A has-a relationship, so it comes through the constructor
         isMigratory = true;                 /// An is-a relationship, so it's safe to hardcode.
         tag = tagID;
      }


      const string Nene::speak() {
         return string( "Nay, nay" );
      }


      void Nene::printInfo() {
         cout << "Nene" << endl;
         cout << "   Tag ID = [" << tag << "]" << endl;
         Bird::printInfo();
      }

}
