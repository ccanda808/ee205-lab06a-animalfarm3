///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   20_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <ctime>

#include "animal.hpp"
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
   cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

const Gender Animal::getRandomGender(){
   srand(time(NULL));
   clock_t c = clock();
   switch((rand()+c) % 3){
   case 0: return MALE; break;
   case 1: return FEMALE; break;
   case 2: return UNKNOWN; break;
   }
   return UNKNOWN;
}

const Color Animal::getRandomColor(){
   srand(time(NULL));
   clock_t c = clock();
   switch((rand()+c) % 6){
   case 0: return BLACK; break;
   case 1: return WHITE; break;
   case 2: return RED; break;
   case 3: return SILVER; break;
   case 4: return YELLOW; break;
   case 5: return BROWN; break;
   }
   return BROWN;
}

const bool Animal::getRandomBool(){
   srand(time(NULL));
   clock_t c = clock();
   switch((rand()+c) % 2){
      case 0: return true; break;
      case 1: return false; break;
   }
   return false;
}

const float Animal::getRandomWeight(const float from, const float to){
   srand(time(NULL));
   return (from + (float(rand())/float((RAND_MAX))*(to-from)));
}

const string Animal::getRandomName(){
   srand(time(NULL));
   clock_t c = clock();
   int length = 4 + ((rand()+c) % 6);
   char u = 65 + ((rand()+c) % (91-65));
   string randomName;
   randomName.push_back(u);
   for(int i = 1; i < length; i++){
      u = 97 + ((rand()+c) % (123-97));
      randomName.push_back(u);
   }
   return string(randomName);
}

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   srand(time(NULL)); clock_t clk = clock();
   int i = (rand() + clk) % 6; //cout << i << " " << rand() + clk << " ";
   switch(i){
      case 0: newAnimal = new Cat    ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 1: newAnimal = new Dog    ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 2: newAnimal = new Nunu   ( Animal::getRandomBool(), RED, Animal::getRandomGender()                      ); break;
      case 3: newAnimal = new Aku    ( Animal::getRandomWeight(15, 20), SILVER, Animal::getRandomGender()           ); break;
      case 4: newAnimal = new Palila ( Animal::getRandomName(), YELLOW, Animal::getRandomGender()                   ); break;
      case 5: newAnimal = new Nene   ( Animal::getRandomName(), BROWN, Animal::getRandomGender()                    ); break;
   }

   return newAnimal;
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK:  return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED:    return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN:  return string("Brown"); break;
   }

   return string("Unknown");
};

} // namespace animalfarm
