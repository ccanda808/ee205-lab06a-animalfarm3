///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Cameron Canda <ccanda@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   20_Mar_2021
///
//////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3" << endl;
   cout << "........................." << endl;

   array<Animal*, 30> animalArray;
   animalArray.fill(NULL);
   for(int i = 0; i < 25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }

   cout << "Array of Animals:" << endl;
   cout << boolalpha;
   cout << "   Is it empty: " << animalArray.empty() << endl;
   cout << "   Number of elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;

   for(Animal* animal : animalArray){
      if(animal == NULL) break;
      cout << animal->speak() << endl;
   }
   
   cout << "xxxxxxxxxxxxxxxxxxxxxxxxx........................." << endl;

   list<Animal*> animalList;
   for(int i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }

   cout << "List of Animals:" << endl;
   cout << boolalpha;
   cout << "   Is it empty: " << animalList.empty() << endl;
   cout << "   Number of elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;

   for(Animal* animal : animalList){
      cout << animal->speak() << endl;
   }

   cout << "xxxxxxxxxxxxxxxxxxxxxxxxx" << endl;

	return 0;
}
